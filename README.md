# Evaluating the Fairness of Neural Collapse in Medical Image Classification
This is the official repository for the MICCAI2024 paper "_Evaluating the Fairness of Neural Collapse in Medical Image Classification_" [Link to the ArXiv will be added soon]

The code in this repository is based on (https://github.com/biomedia-mira/subgroup-separability).

## Abstract
Deep learning has achieved impressive performance across various medical imaging tasks. However, its inherent bias against specific groups hinders its clinical applicability in equitable healthcare systems. A recently discovered phenomenon, Neural Collapse (NC), has shown potential in improving the generalization of state-of-the-art deep learning models. Nonetheless, its implications on bias in medical imaging remain unexplored. Our study investigates deep learning fairness through the lens of NC. We analyze the training dynamics of models as they approach NC when training using biased datasets, and examine the subsequent impact on test performance, specifically focusing on label bias. We find that biased training initially results in different NC configurations across subgroups, before converging to a final NC solution by memorizing all data samples. Through extensive experiments on three medical imaging datasets—PAPILA, HAM10000, and CheXpert—we find that in biased settings, NC can lead to a significant drop in F1 score across all subgroups.


## Data

We use three public datasets in this work: PAPILA, HAM10000, CheXpert.

We follow the same pipelines as https://github.com/biomedia-mira/subgroup-separability to prepare the data.

Modify the paths to your data in the config files under `configs/datamodule`

## Installation

The required packages are in `requirements.txt`. Run `pip install -r requirements.txt` for installation.

## Usage

All experiments can be run through the main file by running `python main.py`. You can adjust the hyperparameters in the YAML files under `configs/`

You can also use the files under `sweeps/` to run multiple experiments with different parameters using wanb sweeps.

The notebook `plots.ipynb` can be used to generate the plots of the analysis. 

## Contact

Kaouther Mouheb, Biomedical Imaging Group Rotterdam, Department of Radiology & Nuclear Medicine, Erasmus MC. 

[k.mouheb@erasmusmc.nl](mailto:k.mouheb@erasmusmc.nl)



