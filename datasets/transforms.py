"""NB. We emprically got a massive speedup by doing these transforms on the GPU during
the training step, as opposed to in the DataLoader as standard."""

import torch
import torch.nn as nn
import torchvision.transforms as transforms
from monai.transforms import Compose, LoadImage, RandRotate90, Resize, NormalizeIntensity

IMAGENET_MEAN = [0.485, 0.456, 0.406]
IMAGENET_STD = [0.229, 0.224, 0.225]

normalise_2d = transforms.Normalize(mean=IMAGENET_MEAN, std=IMAGENET_STD)


def get_transforms_for_eval(dim: int = 2) -> nn.Module:
    """Evaluation (val/test) transforms. Input to the pipeline is a
    RGB uint8 Tensor, output is a normalised float Tensor.
    
    Args:
        dim: dimension of the data (2D or 3D)

    Returns:
        nn.Module: Sequential set of transforms. Call this on the images during the
            training step on GPU.
    """
    
    assert dim == 2 or dim == 3, "Dimension must be either 2 or 3"
    
    if dim == 2:
        transform_list = [
            transforms.ConvertImageDtype(torch.float32),
            transforms.Resize(256),
            transforms.CenterCrop(224),
            normalise_2d,
        ]

        return nn.Sequential(*transform_list)
    
    else:
        val_transforms = Compose(
                            [
                                LoadImage(ensure_channel_first=True),
                                Resize(spatial_size=(96, 116, 96)),
                                NormalizeIntensity(),    
                            ])
        return val_transforms
    


def get_transforms_for_train(augment: bool = True, dim: int = 2) -> nn.Module:
    """Transforms for training data. If augment is True, apply autoaugment policy.

    Input to the pipeline is a RGB uint8 Tensor, output is a normalised float Tensor.

    Args:
        augment (bool, optional): Whether to augment data. If False, simply return
            the eval transforms. Defaults to True.
        dim (int, optional): Dimension of the input data, can be 2D or 3D. Default to 2.

    Returns:
        nn.Module: Sequential set of transforms. Call this on the images during the
            training step on GPU.
    """
    assert dim == 2 or dim == 3, "Dimension must be either 2 or 3"
    
    if not augment:
        return get_transforms_for_eval(dim=dim)

    if dim == 2:
        transform_list = [
            transforms.ConvertImageDtype(torch.float32),
            transforms.RandomResizedCrop(224),
            transforms.RandomRotation(15),
            normalise_2d,
        ]
        return nn.Sequential(*transform_list)
    
    else:
        train_transforms = Compose(
            [
                LoadImage( ensure_channel_first=True),
                Resize( spatial_size=(96, 116, 96)),
                NormalizeIntensity(),
                RandRotate90( prob=0.8, spatial_axes=[0, 2]),
            ])
        return train_transforms

    