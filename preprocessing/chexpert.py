from skimage.io import imread
from skimage.io import imsave
from skimage.transform import resize
import pandas as pd 
import numpy as np
import os
from tqdm import tqdm

df_cxr = pd.read_csv('path/to/train_cheXbert.csv')

img_data_dir = 'path/to/dataset/'

df_cxr['path_preproc'] = df_cxr['Path']

preproc_dir = 'preproc_224x224/'
out_dir = img_data_dir

if not os.path.exists(out_dir + preproc_dir):
    os.makedirs(out_dir + preproc_dir)

for idx, p in enumerate(tqdm(df_cxr['Path'])):

    split =  p.split("/")
    preproc_filename = split[2] + '_' + split[3] + '_' + split[4]
    df_cxr.loc[idx, 'path_preproc'] = preproc_dir + preproc_filename
    out_path = out_dir + preproc_dir + preproc_filename
    
    if not os.path.exists(out_path):
        image = imread(img_data_dir + p)
        image = resize(image, output_shape=(224, 224), preserve_range=True)
        imsave(out_path, image.astype(np.uint8))
        
        print(f"Image saved to {out_path}")