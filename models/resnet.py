import torchvision
import torch.nn as nn
from monai.networks.nets.resnet import resnet34
    
    
    
class ResNet34(nn.Module):
    def __init__(self):
        super(ResNet34, self).__init__()
        resnet = torchvision.models.resnet34(weights=torchvision.models.ResNet34_Weights.DEFAULT)
        # Remove the last fully connected layer
        self.features = nn.Sequential(*list(resnet.children())[:-1], nn.Flatten())

    def forward(self, x):
        x = self.features(x)
        return x
    
    
class ResNet343D(nn.Module):
    def __init__(self):
        super(ResNet343D, self).__init__()
        resnet = resnet34(spatial_dims=3, n_input_channels=1, feed_forward=True, num_classes=2, bias_downsample=False, shortcut_type = "A")
        # Remove the last fully connected layer
        self.features = nn.Sequential(*list(resnet.children())[:-1], nn.Flatten())

    def forward(self, x):
        x = self.features(x)
        return x
    
    
if __name__=="__main__":
    model = ResNet343D()
    print(model)