from typing import Dict
import torch
import numpy as np
import sklearn.metrics as skm


def compute_overall_metrics(
    scores: np.ndarray, labels: np.ndarray,
) -> Dict[str, float]:
    """Compute relevant classification metrics for binary classification tasks, where
    1 is the positive label and 0 is the negative label.

    N.B. Do not use in multi-class classification tasks.

    Args:
        scores (np.ndarray): Array of predicted probability scores, shape (N,2).
            This is usually the output after a Sigmoid/Softmax function.
        labels (np.ndarray): Integer array of labels, shape (N,).


    Returns:
        Dict[str, float]: Dictionary of scalar metrics. Includes:
        - accuracy
        - auc (area under the ROC curve)
    """
    preds = torch.argmax(scores, dim=1)
    assert preds.shape == labels.shape, f"{preds.shape} != {labels.shape}"
    target_names = ["negative_class", "positive_class"]
    report = skm.classification_report(
        y_true=labels, y_pred=preds, output_dict=True, target_names=target_names
    )

    return {
        "accuracy": report["accuracy"],  # type: ignore
        "auc": skm.roc_auc_score(y_true=labels, y_score=scores[:,1]),  # type: ignore
    }


def compute_fairness_metrics(
    scores: np.ndarray,
    labels: np.ndarray,
    attributes: np.ndarray,
) -> Dict[str, float]:
    """Compute relevant fairness metrics for 2-subgroup binary classification tasks,
    where 1 is the positive label and 0 is the negative label.

    N.B. Do not use in multi-class classification tasks or in tasks with more than 2
    subgroups.

    Args:
        scores (np.ndarray): Array of predicted probability scores, shape (N,2).
            This is usually the output after a Sigmoid function.
        labels (np.ndarray): Integer array of labels, shape (N,).
        attributes (np.ndarray): Integer array of subgroup labels, shape (N,).

    Returns:
        Dict[str, float]: Dictionary of scalar metrics. Includes:
        - accuracy for all samples
        - accuracy/balanced accuracy/f1 score for each subgroup
        - disease prevalence for each subgroup
        - prevalence of each subgroup within dataset
    """
    preds = torch.argmax(scores, dim=1)
    assert (
        preds.shape == labels.shape == attributes.shape
    ), f"{preds.shape} != {labels.shape} != {attributes.shape}"
    
    f1_score = skm.f1_score(labels, preds, average='macro')
    accuracy = skm.accuracy_score(labels, preds)
    target_names = ["negative_class", "positive_class"]

    group_0_mask = attributes == 0

    report_group_0 = skm.classification_report(
        y_pred=preds[group_0_mask],
        y_true=labels[group_0_mask],
        output_dict=True,
        target_names=target_names,
    )
    report_group_1 = skm.classification_report(
        y_pred=preds[~group_0_mask],
        y_true=labels[~group_0_mask],
        output_dict=True,
        target_names=target_names,
    )

    num_group_0 = np.sum(attributes == 0)
    prevalence_group_0 = num_group_0 / len(attributes)
    num_group_1 = np.sum(attributes == 1)
    prevalence_group_1 = num_group_1 / len(attributes)

    disease_prevalence_group_0 = np.sum(labels[group_0_mask]) / num_group_0
    disease_prevalence_group_1 = np.sum(labels[~group_0_mask]) / num_group_1

    accuracy_group_0 = report_group_0["accuracy"]  # type: ignore
    accuracy_group_1 = report_group_1["accuracy"]  # type: ignore
    
    baccuracy_group_0 = skm.balanced_accuracy_score(y_pred=preds[group_0_mask], y_true=labels[group_0_mask])
    baccuracy_group_1 = skm.balanced_accuracy_score(y_pred=preds[~group_0_mask], y_true=labels[~group_0_mask])
    
    f1_g0 = skm.f1_score(y_pred=preds[group_0_mask], y_true=labels[group_0_mask], average='macro')
    f1_g1 = skm.f1_score(y_pred=preds[~group_0_mask], y_true=labels[~group_0_mask], average='macro')

    return {
        "accuracy_group_0": accuracy_group_0,
        "accuracy_group_1": accuracy_group_1,
        "balanced_acc_group_0": baccuracy_group_0,
        "balanced_acc_group_1": baccuracy_group_1,
        'f1_g0': f1_g0,
        'f1_g1': f1_g1,
        'f1_score': f1_score,
        "disease_prevalence_group_0": disease_prevalence_group_0,
        "disease_prevalence_group_1": disease_prevalence_group_1,
        "prevalence_group_0": prevalence_group_0,
        "prevalence_group_1": prevalence_group_1,
        "accuracy": accuracy
    }  # type: ignore
    

