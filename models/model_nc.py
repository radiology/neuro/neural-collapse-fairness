from typing import List, Literal, Mapping, Tuple

import torch
import pytorch_lightning as pl
import torch.nn as nn
import torch.nn.functional as F

from .nc_utils import compute_nc, compute_nc1_per_group
from .resnet import ResNet343D, ResNet34

from models import metrics


class BaselineNC(pl.LightningModule):
    """Baseline (ERM) classifier for Binary Classification, with 2 subgroups.
    Can optionally be trained to predict the sensitive attribute instead of the disease.
    """

    def __init__(
        self,
        input_dim:int = 2,
        feat_dim:int = 512,
        lr: float = 3e-4,
        predict_attr: bool = False,
    ) -> None:
        """
        Args:

            lr (float, optional): Learning rate. Defaults to 3e-4.
            predict_attr (bool, optional): Whether to predict the sensitive attribute
                instead of the disease label. When False, the disease prediction
                metrics are stratified by subgroup. Defaults to False.
            feat_dim (int, optional): dimension of the last layer features. Defaults to 512.
            input_dim (int, optional): dimension of the input. can be 2D or 3D. Defaults to 2.
        """
        super().__init__()
        
        assert input_dim in [2, 3], "input_dim must be either 2 or 3"
        if input_dim==2:
            self.backbone = ResNet34()
        else:
            self.backbone = ResNet343D()
        self.lr = lr
        self.predict_attr = predict_attr
        self.feat_dim = feat_dim
        self.prediction_head = nn.Linear(feat_dim, 2, bias = False)  # always binary classification


        # whether to perform the supervised prediction layer information test
        self.SPLIT = False
        self.STEP = ''
    

    def forward(self, x):
        z = self.backbone(x)
        x = self.prediction_head(z)  # note: no sigmoid here
        return x, z # return logits + features

    def configure_optimizers(self):
        if self.SPLIT:
            return torch.optim.Adam(self.parameters(), lr=self.lr)
        
        else: 
            optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
            return {
                "optimizer": optimizer,
                "lr_scheduler": {
                    "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=20, verbose=True, min_lr=5e-7),
                    "monitor": "train/loss",
                    "frequency": 1,
                },
            }
            

    def _step(
        self,
        batch: Tuple[torch.Tensor, torch.Tensor, torch.Tensor],
        
    ) -> Mapping[str, torch.Tensor]:
        imgs, labels, attributes = batch
        logits, feats = self(imgs)

        if self.predict_attr:
            y = attributes
        else:
            y = labels

        # Loss calculation 
        loss = F.cross_entropy(logits.squeeze(1), y)
                            
        return {
            "loss": loss,
            "feats": feats,
            "logits": logits,
            "labels": labels,
            "attributes": attributes,
        }

    def _epoch_end(
        self,
        step_outputs: List[Mapping[str, torch.Tensor]],
        mode: Literal["train", "val", "test"],
    ):
        attributes = (
            torch.cat([x["attributes"] for x in step_outputs]).cpu().detach().numpy()
        )
        logits = torch.cat([x["logits"] for x in step_outputs]).cpu().detach()
        scores = F.softmax(logits, dim=1).detach()
        labels = torch.cat([x["labels"] for x in step_outputs]).cpu().detach().numpy()
    
        features = torch.cat([x["feats"] for x in step_outputs]).detach()

        loss = torch.stack([x["loss"] for x in step_outputs]).mean().item()

        if self.predict_attr:
            metrics_dict = metrics.compute_overall_metrics(
                scores=scores, labels=attributes
            )

        else:
            metrics_dict = metrics.compute_fairness_metrics(
                scores=scores, labels=labels, attributes=attributes
            )
            
            if not self.predict_attr:
                features = torch.cat([x["feats"] for x in step_outputs]).detach()
                labels = torch.tensor(labels, device=features.device)
                scores = torch.tensor(scores, device=features.device)
                attributes = torch.tensor(attributes, device=features.device)
                nc1, nc2, nc3, nc4 = compute_nc(features, 2, labels, scores, self.prediction_head.weight.float())
                nc1_g0, nc1_g1 = compute_nc1_per_group(features, 2, labels, attributes)
                metrics_dict['nc1'] = nc1
                metrics_dict['nc2'] = nc2
                metrics_dict['nc3'] = nc3
                metrics_dict['nc4'] = nc4
                metrics_dict['nc1_g0'] = nc1_g0
                metrics_dict['nc1_g1'] = nc1_g1
                    

        metrics_dict["loss"] = loss

        # modify the keys to include the mode, also convert back to torch.Tensor
        torch_metrics_dict_with_mode = {
            f"{self.STEP}{mode}/{k}": v
            for k, v in metrics_dict.items()
        }
        self.log_dict(torch_metrics_dict_with_mode, on_epoch=True, prog_bar=True)
        return torch_metrics_dict_with_mode

    def set_SPLIT(self, step):
        """SPLIT is the supervised prediction layer information test. It involves
        freezing the backbone and re-training the prediction layer to see if the
        representations encode subgroup information. Call this method, then train
        and test again to obtain SPLIT metrics."""

        for param in self.backbone.parameters():
            param.requires_grad = False
        for param in self.prediction_head.parameters():
            param.requires_grad = True
        self.predict_attr = True
        self.SPLIT = True
        self.STEP = step
        

    def training_step(self, batch, batch_idx):
        del batch_idx
        return self._step(batch)

    def training_epoch_end(self, step_outputs: List[Mapping[str, torch.Tensor]]):
        self._epoch_end(step_outputs, "train")

    def validation_step(self, batch, batch_idx):
        del batch_idx
        return self._step(batch)

    def validation_epoch_end(self, step_outputs: List[Mapping[str, torch.Tensor]]):
        if self.trainer.state.stage == "sanity_check":
            return
        self._epoch_end(step_outputs, "val")

    def test_step(self, batch, batch_idx):
        del batch_idx
        return self._step(batch)

    def test_epoch_end(self, step_outputs: List[Mapping[str, torch.Tensor]]):
        self._epoch_end(step_outputs, "test")