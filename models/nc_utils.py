from scipy.stats import ortho_group
import torch
import math
import numpy as np
from scipy.sparse.linalg import svds


def compute_nc(h_all, num_classes, labels, scores, weights):
    """Computes metrics of NC1-4 properties

    Args:
        h_all (torch.Tensor): Last layer features of all samples in the data split, shape (N, feat_dim).
            This is usually the output of the model's backbone.
        num_classes (int): Number of classes (in our case 2).
        labels (torch.Tensor): Integer array of labels, shape (N,).
        scores (torch.Tensor): Probabilities predicted by the model for each class, shape (N, 2). 
            This is usually the output of Sigmoid/Softmax layer.
        
    Returns:
        Tuple: Tuple of four scalar metrics one for each NC property in this order NC1, NC2, NC3, NC4
    """
    preds = torch.argmax(scores, dim=1)
    num_samples = len(labels)
    
    gmeans = torch.mean(h_all, dim=0, keepdim=True).T
    sigma_w = 0
    mean = [0 for _ in range(num_classes)]
    for c in range(num_classes):
        idxs = (labels == c).nonzero(as_tuple=True)[0]
        h_c = h_all[idxs, :]
        mean[c] = torch.mean(h_c, dim=0, keepdim=False)
        z = h_c - mean[c].unsqueeze(0)
        batch_size = 1000
        if z.size(0) > 1000:
            cov = torch.zeros(z.size(1), z.size(1), device=z.device)
            for i in range(0, len(z), batch_size):
                if i+batch_size < z.size(0):
                    batch_z = z[i:i+batch_size, :]
                else:
                    batch_z = z[i:, :]
                cov += torch.matmul(batch_z.unsqueeze(-1), batch_z.unsqueeze(1)).sum(dim=0)   
        else:
            cov = torch.matmul(z.unsqueeze(-1), z.unsqueeze(1)).sum(dim=0)
            
        sigma_w += cov
    if z.size(0) > 1000:
        del batch_z
        del z
    del cov
        
    fmeans = torch.stack(mean).T
    fmeans_ = fmeans - gmeans
    
    sigma_b =  torch.matmul(fmeans_, fmeans_.T) / num_classes 
    sigma_b = sigma_b.cpu().numpy()
    sigma_w = sigma_w / h_all.shape[0]
    sigma_w = sigma_w.cpu().numpy()
    eigvec, eigval, _ = svds(sigma_b.astype(float), k=num_classes-1)
    inv_Sb = eigvec @ np.diag(eigval**(-1)) @ eigvec.T 
    nc1 = np.trace(sigma_w @ inv_Sb)
    del sigma_w
    del sigma_b
    del inv_Sb
    del eigvec
    del eigval
    
    nc2 = abs(1-torch.linalg.cond(fmeans))
    
    normalized_fmeans = fmeans_ / torch.norm(fmeans_,'fro')
    normalized_weights = weights.T / torch.norm(weights.T,'fro')
    nc3 = (torch.norm(normalized_weights - normalized_fmeans)**2).item()
    del normalized_fmeans
    del normalized_weights
    
    NCC_scores = torch.stack([torch.norm(h_all[i,:] - fmeans.T, dim=1) for i in range(h_all.shape[0])])
    NCC_pred = torch.argmin(NCC_scores, dim=1)
    NCC_match_net = sum(NCC_pred==preds).item()
    nc4 = 1-NCC_match_net/num_samples
    
    return nc1, nc2, nc3, nc4


def compute_nc1_per_group(h_all, num_classes, labels, attributes):
    """Computes NC1 metric for each subgroup. Causion: assumes only 2 subgroups. Do not use for more

    Args:
        h_all (torch.Tensor): Last layer features of all samples in the data split, shape (N, feat_dim).
            This is usually the output of the model's backbone.
        num_classes (int): Number of classes (in our case 2).
        labels (torch.Tensor): Integer tensor of labels, shape (N,).
        attributes (torch.Tensor):  Integer tensor of attributes representing the group of each sample, shape (N,).
        
    Returns:
        Tuple: Tuple of two scalar metrics representing NC1 for G1 and NC1 for G2
    """
    nc1_dict = []
    mean = [0 for _ in range(num_classes)]
    for k in range(num_classes):
        idxs = (labels == k).nonzero(as_tuple=True)[0]
        h_k = h_all[idxs, :]
        mean[k] = torch.mean(h_k, dim=0, keepdim=False)
    
    means = torch.stack(mean) # Means
    
    muG = torch.mean(h_all, dim=0, keepdim=True).T # Global mean
    M = means.T # Means
    M_ = M - muG # Shifted Means
    
    Sb = torch.matmul(M_, M_.T) / num_classes
    Sb = Sb.cpu().numpy()
    
    for a in range(2): # for each group
        idxs_a = (attributes == a).nonzero(as_tuple=True)[0]
        h_a = h_all[idxs_a, :]
        labels_a = labels[idxs_a]
        Sw = 0 
        for k in range(num_classes): # For Each class
            idx_k = (labels_a == k).nonzero(as_tuple=True)[0]
            h_ka = h_a[idx_k,: ]
            z = h_ka - means[k].unsqueeze(0)
            batch_size = 1000
            if z.size(0) > 1000:
                cov = torch.zeros(z.size(1), z.size(1), device=z.device)
                for i in range(0, len(z), batch_size):
                    if i+batch_size < z.size(0):
                        batch_z = z[i:i+batch_size, :]
                    else:
                        batch_z = z[i:, :]
                    cov += torch.matmul(batch_z.unsqueeze(-1), batch_z.unsqueeze(1)).sum(dim=0)   
            else:
                cov = torch.matmul(z.unsqueeze(-1), z.unsqueeze(1)).sum(dim=0)
            Sw += cov
        if z.size(0) > 1000:
            del batch_z
            del z
        del cov
        Sw = Sw / len(idxs_a)
        Sw = Sw.cpu().numpy()
        eigvec, eigval, _ = svds(Sb, k=num_classes-1)
        inv_Sb = eigvec @ np.diag(eigval**(-1)) @ eigvec.T 
        Sw_invSb = np.trace(Sw @ inv_Sb)
        nc1_dict.append(Sw_invSb)
        
    return nc1_dict[0], nc1_dict[1]
            
            
            
    
    
    