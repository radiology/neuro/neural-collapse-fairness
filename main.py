import logging

import hydra
import pytorch_lightning as pl
import torch
from hydra.core.hydra_config import HydraConfig
from omegaconf import DictConfig
from pytorch_lightning.loggers import WandbLogger  # type: ignore
from pytorch_lightning.loggers import CSVLogger, TensorBoardLogger
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint

from datasets.dataset import FairnessDataModule
import models.model_nc

import os
from pytorch_lightning.callbacks import Callback
from pytorch_lightning.utilities import rank_zero_only

# Callback mimics Early Stopping, only saves the model in out_dir/best.ckpt but does not stop
class SaveModelCallback(Callback):
    def __init__(self, monitor="val_loss", mode="min", patience=5, save_dir="./saved_models"):
        super().__init__()
        self.monitor = monitor
        self.mode = mode
        self.patience = patience
        self.save_dir = save_dir
        self.saved = False
        os.makedirs(self.save_dir, exist_ok=True)
        if mode == "min":
            self.best_score = float("inf")
        else:
            self.best_score = float("-inf")
        self.counter = 0

    def on_validation_end(self, trainer, pl_module):
        if self.saved:
            return
        
        metrics = trainer.callback_metrics
        if metrics is None or self.monitor not in metrics:
            return

        score = metrics[self.monitor]
        
        if (self.mode == "min" and score < self.best_score) or (self.mode == "max" and score > self.best_score):
            self.best_score = score
            self.counter = 0
            
        else:
            self.counter += 1
            if self.counter >= self.patience:
                self.save_checkpoint(trainer, pl_module)
                self.saved = True
                
    @rank_zero_only
    def save_checkpoint(self, trainer, pl_module):
        filepath = os.path.join(self.save_dir, f"best.ckpt")
        trainer.save_checkpoint(filepath, weights_only=True)
        logging.info(f"Model saved at {filepath} at epoch {pl_module.current_epoch}")
        


@hydra.main(config_path="configs/", config_name="config_nc.yaml", version_base="1.2")
def main(cfg: DictConfig) -> None:
    output_dir = HydraConfig.get()["runtime"]["output_dir"]  # type: ignore
    pl.seed_everything(cfg.rng, workers=True)

    # Initialize the model and dataset based on the parameters given in cfg
    dm: FairnessDataModule = hydra.utils.instantiate(cfg.datamodule)
    module: models.model_nc.BaselineNC = hydra.utils.instantiate(cfg.model)

    # Set up mixed precision if specified in cfg
    precision = 32
    if cfg.mixed_precision:
        torch.set_float32_matmul_precision("medium")
        precision = 16

    # Set up loggers
    loggers = []
    if cfg.csv_logger:
        csv_logger = CSVLogger(save_dir=output_dir)
        loggers.append(csv_logger)
    if cfg.wandb_logger:
        wandb_logger = WandbLogger(save_dir=output_dir, project=cfg.project_name)
        wandb_logger.watch(module, log="all", log_freq=100)
        loggers.append(wandb_logger)
    if cfg.tb_logger:
        tb_logger = TensorBoardLogger(save_dir=output_dir)  # type: ignore
        loggers.append(tb_logger)

    logging.info(f"Subgroups available in this dataset: {dm.get_subgroup_names()}")

    # Initialize the training callbacks
    callbacks = []
    if cfg.model.predict_attr and cfg.early_stopping:
        # Standard early stopping used when training models to predict the attributes from the raw data
        es = EarlyStopping(
                monitor="val/loss",
                patience=cfg.early_stopping,
                mode="min",
                verbose=True,
                check_finite=True,
                strict=True,
            )
        callbacks.append(es)
    if not cfg.model.predict_attr:
        # Save early-stage and final-stage model using early stopping when training the model to predict the disease
        if cfg.early_stopping:
            #Saves model as in early stopping
            es = SaveModelCallback(
                monitor="val/loss", 
                patience=cfg.early_stopping,
                mode="min",
                save_dir=output_dir)       
            callbacks.append(es)
        # saves last mode;
        chkpt = ModelCheckpoint(
                                monitor=None, 
                                dirpath=output_dir,  
                                filename='model-{epoch}', 
                                save_last=True,  
                                save_top_k=0,
                                save_weights_only = True,
                            )
        callbacks.append(chkpt)
    # Pytorch lightning trainer 
    trainer = pl.Trainer(
        max_epochs=cfg.num_epochs,
        accelerator="gpu",
        logger=loggers,
        enable_checkpointing=cfg.enable_checkpointing,
        precision=precision,
        default_root_dir=output_dir,
        callbacks=callbacks,
        enable_progress_bar = False,
    )
    trainer.fit(module, dm) # train
    trainer.test(module, datamodule=dm) # test

    if cfg.split_test and not cfg.model.predict_attr:
        # perform SPLIT test by re-training to predict attribute with frozen backbone
        # here the model has the weights of the final stage (max epochs)
        callbacks = []
        if cfg.early_stopping:
            es = EarlyStopping(
                monitor="split_val/loss",
                patience=cfg.early_stopping,
                mode="min",
                verbose=True,
                check_finite=True,
                strict=True,
            )
            callbacks.append(es)
            
        split_trainer = pl.Trainer(
            max_epochs=cfg.num_epochs,
            accelerator="gpu",
            logger=loggers,
            enable_checkpointing=False,
            precision=precision,
            default_root_dir=output_dir,
            callbacks=callbacks,
            enable_progress_bar = False,
        )
        print()
        print("SPLIT of LAST model")
        module.set_SPLIT("split_")
        split_trainer.fit(module, dm)
        split_trainer.test(module, datamodule=dm)
        
        
        
        print()
        print("SPLIT of BEST model")
        callbacks = []
        if cfg.early_stopping:
            es = EarlyStopping(
                monitor="bsplit_val/loss",
                patience=cfg.early_stopping,
                mode="min",
                verbose=True,
                check_finite=True,
                strict=True,
            )
            callbacks.append(es)
        
        # perform SPLIT test by re-training to predict attribute with frozen backbone
        # here we load the weights saved with early stopping (best.ckpt)
        # if early stopping was not reached we use last.ckpt 
        split_trainer = pl.Trainer(
            max_epochs=cfg.num_epochs,
            accelerator="gpu",
            logger=loggers,
            enable_checkpointing=False,
            precision=precision,
            default_root_dir=output_dir,
            callbacks=callbacks,
            enable_progress_bar = False,
        )
        

        model_path = os.path.join(output_dir, f"best.ckpt")
        if not os.path.exists(model_path):
            print("loading from last")
            model_path = os.path.join(output_dir, f"last.ckpt")
        module.load_from_checkpoint(model_path)
        module.set_SPLIT("bsplit_")
        split_trainer.fit(module, dm)
        split_trainer.test(module, datamodule=dm)


if __name__ == "__main__":
    main()
